module jpeersapp.jpeers {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;
    requires org.apache.logging.log4j;
    requires org.apache.logging.log4j.core;

    opens jpeersapp.frontend to javafx.fxml;
    exports jpeersapp.frontend;
    exports jpeersapp.frontend.udp_peers;
    opens jpeersapp.frontend.udp_peers to javafx.fxml;
    exports jpeersapp.frontend.tabs;
    opens jpeersapp.frontend.tabs to javafx.fxml;
    exports jpeersapp.frontend.generic_peers;
    opens jpeersapp.frontend.generic_peers to javafx.fxml;
    exports jpeersapp.frontend.tabs.peer_choice;
    opens jpeersapp.frontend.tabs.peer_choice to javafx.fxml;
    exports jpeersapp.frontend.tabs.logging;
    opens jpeersapp.frontend.tabs.logging to javafx.fxml;
}