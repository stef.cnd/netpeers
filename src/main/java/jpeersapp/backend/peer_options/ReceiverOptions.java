package jpeersapp.backend.peer_options;


/**
 * Class that models the set of options belonging to
 * a receiver peer;
 */
public class ReceiverOptions extends DefaultPeerOptions {
    private int receiverBufferSize;
    private int recvFrequency;
    private int backlog;


    /**
     * Constructor
     */
    public ReceiverOptions() {
        this.receiverBufferSize = DEFAULT_PAYLOAD_SIZE;
        this.recvFrequency = DEFAULT_RECEIVE_FREQUENCY;
        this.backlog = DEFAULT_BACKLOG;
    }


    /**
     * Get the receiver buffer size;
     */
    public int getReceiverBufferSize() {
        return this.receiverBufferSize;
    }


    /**
     * Get the receiver frequency;
     */
    public int getRecvFrequency() {
        return this.recvFrequency;
    }


    /**
     * Get the receiver backlog;
     */
    public int getBacklog() {
        return this.backlog;
    }


    /**
     * Set the receiver buffer size;
     */
    public void setReceiverBufferSize(String newPayload) {
        if (!newPayload.isEmpty()) {
            this.receiverBufferSize = Integer.parseInt(newPayload);
        }
    }


    /**
     * Set the receiver frequency;
     */
    public void setRecvFrequency(String newFrequency) {
        if (!newFrequency.isEmpty()) {
            this.recvFrequency = Integer.parseInt(newFrequency);
        }
    }


    /**
     * Set the receiver buffer size;
     */
    public void setBacklog(String newBacklog) {
        if (!newBacklog.isEmpty()) {
            this.backlog = Integer.parseInt(newBacklog);
        }
    }
}