package jpeersapp.backend.peer_options;


/**
 * Abstract class that models the set of options belonging to
 * a generic peer;
 */
public abstract class DefaultPeerOptions {

    protected static final int DEFAULT_SEND_FREQUENCY = 1000;
    protected static final int DEFAULT_RECEIVE_FREQUENCY = 1000;
    protected static final int DEFAULT_INCREMENTAL_COUNTER = 0;
    protected static final int DEFAULT_ARBITRARY_SIZE = 0;
    protected static final int DEFAULT_PAYLOAD_SIZE = 10;
    protected static final int DEFAULT_BACKLOG = 50;
}
