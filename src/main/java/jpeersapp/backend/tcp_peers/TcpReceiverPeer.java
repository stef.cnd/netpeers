package jpeersapp.backend.tcp_peers;


import java.io.Closeable;
import java.net.Socket;
import java.io.IOException;
import java.net.ServerSocket;
import java.io.DataInputStream;
import java.util.concurrent.Executors;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import jpeersapp.backend.peer_options.ReceiverOptions;


/**
 * Class that models a receiver TCP peer;
 */
public class TcpReceiverPeer extends TcpPeer {

    private ServerSocket receiverSocket;
    public ReceiverOptions options;
    private int noOfBytesRead;
    private byte[] receiveBuffer;
    private DataInputStream in;
    private Socket clientSocket;
    private static final Logger tcpReceiverLogger = LogManager.getLogger(TcpReceiverPeer.class.getName());


    /**
     * Constructor
     */
    public TcpReceiverPeer() {
        super();

        this.options = new ReceiverOptions();
        this.receiveBuffer = new byte[this.options.getReceiverBufferSize()];
    }


    /**
     * Set the receiving related actions;
     * Create a new thread for the current peer;
     */
    public void startServer() {
        this.executor = Executors.newCachedThreadPool();

        Runnable task = () -> {
            try {
                this.receiveBuffer = new byte[this.options.getReceiverBufferSize()];
                this.receiverSocket = new ServerSocket(this.port, this.options.getBacklog(), this.address);
                this.clientSocket = receiverSocket.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }

            while(!this.executor.isShutdown()) {
                try {
                    this.in = new DataInputStream(this.clientSocket.getInputStream());
                    this.noOfBytesRead = this.in.read(this.receiveBuffer);
                    tcpReceiverLogger.info(buildLogMessage());
                    Thread.sleep(this.options.getRecvFrequency());
                } catch (InterruptedException | IOException e) {
                    System.out.println(e.getMessage());
                }
            }
        };

        this.executor.execute(task);
    }


    /**
     * Close the socket associated with the peer and
     * shutdown the related thread (if any);
     */
    public void closePeer() {
        super.closePeer(this.receiverSocket);
    }

    /**
     * Return the sender socket;
     */
    public Closeable getSocket() {
        return this.receiverSocket;
    }


    /**
     * Assembles the login message associated with a single
     * UDP receive action. The format is:
     * yyyy-MM-dd HH:mm:ss.SSS - receiverIP:receiverPort  <--  senderIp:senderPort  Len=packetLen
     */
    protected String buildLogMessage() {

        return String.format("%1$-25s %2$-25s %3$-20s %4$s",
                "Src: " + this.clientSocket.getInetAddress().getHostAddress(),
                "Dst: " + this.address.getHostAddress(),
                this.clientSocket.getPort() + " --> " + this.port,
                "Len=" + this.noOfBytesRead);
    }
}
