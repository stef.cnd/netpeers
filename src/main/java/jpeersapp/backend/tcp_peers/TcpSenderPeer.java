package jpeersapp.backend.tcp_peers;

import java.net.*;
import java.io.Closeable;
import java.math.BigInteger;
import java.io.IOException;
import java.util.concurrent.Executors;
import org.apache.logging.log4j.Logger;
import java.nio.charset.StandardCharsets;
import org.apache.logging.log4j.LogManager;
import jpeersapp.backend.peer_options.SenderOptions;


/**
 * Class that models a TCP sender peer;
 */
public class TcpSenderPeer extends TcpPeer {

    private byte[] sendBuffer;
    private Socket senderSocket;
    public SenderOptions options;
    private static final Logger tcpSenderLogger = LogManager.getLogger(TcpSenderPeer.class.getName());


    /**
     * Constructor
     */
    public TcpSenderPeer() {
        super();

        this.options = new SenderOptions();
    }


    /**
     * Initialise a new TCP socket;
     */
    public void initialiseSocket() {
        try {
            this.senderSocket = new Socket(this.address, this.port);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        try {
            this.out = this.senderSocket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Set the sender buffer data of the TCP peer (raw content);
     */
    public void setSendBufferData(byte[] buffer) {
        this.sendBuffer = buffer;
    }


    /**
     * Set a new size of the sender buffer;
     * Note: this is used only in case of sending mode "Arbitrary Size";
     */
    public void setArbitrarySize(int newSize) {
        this.sendBuffer = new byte[newSize];
    }


    /**
     * Increase the counter value;
     * Note this is used only in case of sending mode "Incremental Counter";
     */
    public void increaseCounter(int counter) {
        this.options.setIncrementalCounter(counter+1);
        String counterString = Integer.toString(counter);
        this.setSendBufferData(counterString.getBytes(StandardCharsets.UTF_8));
    }


    /**
     * Set the sending related actions;
     * Create a new thread for the current peer;
     */
    public void startSending() {
        this.executor = Executors.newCachedThreadPool();

        Runnable task = () -> {
            while(!this.executor.isShutdown()) {
                try {
                    switch (this.options.getSendMode()) {
                        case "Custom Payload" -> { /* placeholder */ }
                        case "Incremental Counter" -> {
                            this.increaseCounter(this.options.getIncrementalCounter());
                            BigInteger cnt = BigInteger.valueOf(this.options.getIncrementalCounter());
                            this.setSendBufferData(cnt.toByteArray());
                            System.out.println(this.sendBuffer[0]);
                        }
                        case "Arbitrary Size" -> { /* placeholder */ }
                        default -> { System.out.println("WARNING: Sending mode not valid"); }
                    }

                    out.write(this.sendBuffer);
                    tcpSenderLogger.info(buildLogMessage());
                    Thread.sleep(this.options.getSendFrequency());
                } catch (IOException | InterruptedException e) {
                    e.getMessage();
                }
            }
        };

        this.executor.execute(task);
    }


    /**
     * Close the socket associated with the peer and
     * shutdown the related thread (if any);
     */
    public void closePeer() {
        super.closePeer(this.senderSocket);
    }


    /**
     * Return the sender socket;
     */
    public Closeable getSocket() {
        return this.senderSocket;
    }


    /**
     * Assembles the login message associated with a single
     * UDP send action. The format is:
     * yyyy-MM-dd HH:mm:ss.SSS - senderIP:senderPort  --> receiverIp:receiverPort  Len=packetLen
     */
    protected String buildLogMessage() {

        return String.format("%1$-25s %2$-25s %3$-20s %4$s",
                "Src: " + this.senderSocket.getLocalAddress().getHostAddress(),
                "Dst: " + this.address.getHostAddress(),
                this.senderSocket.getLocalPort() + " --> " + this.port,
                "Len=" + this.sendBuffer.length);
    }
}
