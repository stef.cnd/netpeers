package jpeersapp.backend.tcp_peers;


import java.io.InputStream;
import java.io.OutputStream;
import jpeersapp.backend.generic_peer.AbstractPeer;


/**
 * Abstract class that models a generic TCP peer;
 */
public abstract class TcpPeer extends AbstractPeer {

    protected InputStream in;
    protected OutputStream out;


    /**
     * Constructor
     */
    public TcpPeer() {
        address = null;
        port = 0;
        in = null;
        out = null;
    }
}