package jpeersapp.backend.generic_peer;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;


/**
 * Abstract class that models a generic peer;
 */
public abstract class AbstractPeer {

    protected ExecutorService executor;
    protected InetAddress address;
    protected int port;


    /**
     * Set the peer address;
     * Method called from Gui specific peer class;
     */
    public void setPeerAddress(String readAddress) {
        try {
            this.address = InetAddress.getByName(readAddress);
        } catch (UnknownHostException e) {
            System.out.println(e.getMessage());
        }
    }


    /**
     * Set the peer port;
     * Method called from Gui specific peer class;
     */
    public void setPeerPort(int port) {
        try{
            this.port = port;
        }catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }


    /**
     * Close the socket associated with the peer and
     * shutdown the related thread (if any);
     */
    public void closePeer(Closeable socket) {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
            this.executor.shutdown();
        }
    }


    /**
     * Return the socket object of the peer;
     */
    public abstract Closeable getSocket();


    /**
     * Build a log message assembling the necessary information;
     */
    protected abstract String buildLogMessage();
}