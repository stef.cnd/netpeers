package jpeersapp.backend.udp_peers;


import java.net.*;
import java.io.IOException;
import java.util.concurrent.Executors;
import jpeersapp.backend.peer_options.ReceiverOptions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Class that models a receiver UDP peer;
 */
public class UdpReceiverPeer extends UdpPeer {

    private byte[] receiveBuffer;
    public ReceiverOptions options;
    private static final Logger udpReceiverLogger = LogManager.getLogger(UdpReceiverPeer.class.getName());


    /**
     * Constructor
     */
    public UdpReceiverPeer() {
        super();

        this.options = new ReceiverOptions();
        this.receiveBuffer = new byte[this.options.getReceiverBufferSize()];
    }


    /**
     * Initialise a datagram receiver socket;
     */
    @Override
    public void initialiseSocket() {
        try {
            this.socket = new DatagramSocket(this.port, this.address);
        } catch (SocketException e) {
            System.out.println(e);
        }
    }


    /**
     * Re-initialise the receiver buffer with the current size;
     * Initialise a datagram packet that is used to receive data;
     */
    @Override
    public void initialisePacket() {
        this.receiveBuffer = new byte[this.options.getReceiverBufferSize()];
        this.packet = new DatagramPacket(this.receiveBuffer, this.receiveBuffer.length);
    }


    /**
     * Set the receiving related actions;
     * Create a new thread for the current peer;
     */
    public void startReceiving() {
        this.executor = Executors.newCachedThreadPool();

        Runnable task = () -> {
            while (!this.executor.isShutdown()) {
                try {
                    this.socket.receive(this.packet);
                    udpReceiverLogger.info(buildLogMessage());
                    Thread.sleep(this.options.getRecvFrequency());
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        this.executor.execute(task);
    }


    /**
     * Assembles the login message associated with a single
     * UDP receive action. The format is:
     * yyyy-MM-dd HH:mm:ss.SSS - receiverIP:receiverPort  <--  senderIp:senderPort  Len=packetLen
     */
    protected String buildLogMessage() {

        return String.format("%1$-25s %2$-25s %3$-20s %4$s",
                "Src: " + this.packet.getAddress().getHostAddress(),
                "Dst: " + this.socket.getLocalAddress().getHostAddress(),
                this.packet.getPort() + " --> " + this.socket.getLocalPort(),
                "Len=" + this.packet.getLength());
    }
}