package jpeersapp.backend.udp_peers;


import java.io.Closeable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import jpeersapp.backend.generic_peer.AbstractPeer;


/**
 * Abstract class that models a generic UDP peer;
 */
public abstract class UdpPeer extends AbstractPeer {

    protected DatagramSocket socket;
    protected DatagramPacket packet;


    /**
     * Constructor
     */
    public UdpPeer() {
        this.socket = null;
        this.packet = null;
        this.address = null;
        this.port = 0;
    }


    /**
     * Close the socket associated with the peer and
     * shutdown the related thread (if any);
     */
    public void closePeer() {
        super.closePeer(this.socket);
    }


    /**
     * Return the sender socket;
     */
    public Closeable getSocket() {
        return this.socket;
    }


    /**
     * Initialise a new datagram socket;
     */
    protected abstract void initialiseSocket();


    /**
     * Initialise a new datagram packet;
     */
    protected abstract void initialisePacket();
}