package jpeersapp.launcher;

import jpeersapp.frontend.JPeersApplication;
import java.lang.String;

public class Launcher {
    public static void main(String[] args) {
        JPeersApplication.main(args);
    }
}
