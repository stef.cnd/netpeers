package jpeersapp.frontend;


import javafx.stage.Stage;
import javafx.scene.Scene;
import java.io.IOException;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.application.Application;
import jpeersapp.frontend.tabs.TcpTab;
import jpeersapp.frontend.tabs.UdpTab;
import jpeersapp.frontend.tabs.GenericTab;


public class JPeersApplication extends Application {
    @Override
    public void start(Stage window) throws IOException {

        BorderPane bp = new BorderPane();

        TabPane tabs = new TabPane();

        GenericTab udpTab = new UdpTab("Udp");
        GenericTab tcpTab = new TcpTab("Tcp");

        tabs.getTabs().addAll(udpTab, tcpTab);
        bp.setTop(tabs);

        Scene scene = new Scene(bp, 850, 718);
        scene.getStylesheets().add("/jpeers.css");

        window.setTitle("JPeers 1.1");
        window.setScene(scene);
        window.setResizable(false);
        window.show();
    }

    public static void main(String[] args) {
        launch();
    }
}