package jpeersapp.frontend.options_window;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;


/**
 * This class models the receiver part of the Options Window;
 */
public class GuiReceiverOptions extends GuiPeerOptions {

    private final Label recvFrequencyLabel;
    private final TextField recvFrequencyField;
    private final Label receiverBufferSizeLabel;
    private final TextField receiverBufferSizeField;
    private final Label backlogLabel;
    private final TextField backlogField;


    /**
     * Constructor
     */
    public GuiReceiverOptions() {
        super();

        this.guiPeerOptionsName = new Label("Receiver Options");
        this.recvFrequencyLabel = new Label ("Receive Frequency (ms)");
        this.recvFrequencyField = new TextField();
        this.receiverBufferSizeLabel = new Label("Max Payload (bytes) ");
        this.receiverBufferSizeField = new TextField();
        this.backlogLabel = new Label("Backlog");
        this.backlogField = new TextField();

        this.addItemsToPeerOptions();
    }


    /**
     * Set the receiver options look;
     */
    @Override
    public void setPeerOptionsLook() {
        super.setPeerOptionsLook();

        this.recvFrequencyField.setMaxWidth(100);
        this.receiverBufferSizeField.setMaxWidth(100);
        this.backlogField.setMaxWidth(100);
    }


    /**
     * Add items to the receiver options;
     */
    @Override
    protected void addItemsToPeerOptions() {
        this.optionsGrid.add(this.guiPeerOptionsName, 0, 0);
        this.optionsGrid.add(this.recvFrequencyLabel, 0, 1);
        this.optionsGrid.add(this.recvFrequencyField, 1, 1);
        this.optionsGrid.add(this.receiverBufferSizeLabel, 0, 2);
        this.optionsGrid.add(this.receiverBufferSizeField, 1, 2);
        this.optionsGrid.add(this.backlogLabel, 0, 3);
        this.optionsGrid.add(this.backlogField, 1, 3);
    }


    /**
     * Get the current frequency at which packets are received;
     */
    public String gui_getRecvFrequency() {
        return this.recvFrequencyField.getText();
    }


    /**
     * Get the current maximum size of the receiver buffer;
     */
    public String gui_getReceiverBufferSize() {
        return this.receiverBufferSizeField.getText();
    }


    /**
     * Get the current backlog;
     */
    public String gui_getBacklog() {
        return this.backlogField.getText();
    }
}