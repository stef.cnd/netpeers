package jpeersapp.frontend.generic_peers;


import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import jpeersapp.backend.generic_peer.AbstractPeer;
import jpeersapp.frontend.options_window.OptionsWindow;
import jpeersapp.frontend.tabs.peer_choice.PeerChoice;


/**
 * Abstract class that models the GUI equivalent of an AbstractPeer;
 * Each GuiPeer is modeled as an HBox which functions as a container
 * for all the necessary nodes;
 */
public abstract class GuiPeer {

    protected HBox guiPeerContainer;
    private final Label idLabel;
    private final Label addressLabel;
    protected TextField addressField;
    private final Label portLabel;
    protected TextField portField;
    protected final OptionsWindow optionsWindow;
    public final PeerChoice choice;
    public Button sendButton;
    public Button stopButton;
    public Button recvButton;
    protected GuiPeerState state;


    /**
     * Constructor
     */
    protected GuiPeer(Integer peerId, PeerChoice choice) {
        this.guiPeerContainer = new HBox();
        this.idLabel = new Label("#" + Integer.toString(peerId));
        this.addressLabel = new Label("Address");
        this.addressField = new TextField();
        this.portLabel = new Label("Port");
        this.portField = new TextField();
        this.choice = choice;
        this.optionsWindow = new OptionsWindow(this);
        this.state = GuiPeerState.IDLE;
    }


    /**
     * Set the look of the guiPeer container and of the items in it;
     */
    protected void setGuiPeerLook() {
        this.guiPeerContainer.setSpacing(10);
        this.guiPeerContainer.setAlignment(Pos.CENTER_LEFT);

        this.idLabel.setMinWidth(35);
        this.addressField.setPrefWidth(130);
        this.portField.setPrefWidth(60);
    }


    /**
     * Add items to the guiPeer container;
     */
    protected void addItemsToGuiPeer() {

        guiPeerContainer.getChildren().addAll(this.idLabel,
                this.addressLabel,
                this.addressField,
                this.portLabel,
                this.portField);
    }


    /**
     * Get the guiPeer container in which all items are stored;
     */
    public HBox getGuiPeerContainer() {
        return guiPeerContainer;
    }


    /**
     * Collect user input on peer properties;
     * Set concrete Peer properties (backend);
     */
    protected abstract void collectAndSetPeerProperties();


    /**
     * Apply new options to the concrete Peer (backend);
     */
    public abstract void applyNewOptions();


    /**
     * Get the actual backend peer encapsulated in the guiPeer;
     */
    public abstract AbstractPeer getPeer();
}