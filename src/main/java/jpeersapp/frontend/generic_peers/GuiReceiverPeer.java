package jpeersapp.frontend.generic_peers;


import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.Region;
import jpeersapp.frontend.tabs.peer_choice.PeerChoice;


/**
 * Abstract class that models the GUI equivalent of a generic
 * sender peer;
 */
public abstract class GuiReceiverPeer extends GuiPeer {

    protected Region leftRegion;


    /**
     * Constructor
     */
    protected GuiReceiverPeer(Integer peerId, PeerChoice choice) {
        super(peerId, choice);

        this.recvButton = new Button("Recv");
        this.stopButton = new Button("Stop");
        this.leftRegion = new Region();

        this.setGuiPeerLook();
    }


    /**
     * Set the look of the sender container and of the items in it;
     */
    @Override
    protected void setGuiPeerLook() {
        super.setGuiPeerLook();

        this.recvButton.setMinWidth(60.0);
        this.stopButton.setMinWidth(60.0);
        this.recvButton.setAlignment(Pos.BASELINE_CENTER);
        this.stopButton.setAlignment(Pos.BASELINE_CENTER);
        this.leftRegion.setMinWidth(233);
    }


    /**
     * Add items to the receiver peer container;
     */
    @Override
    public void addItemsToGuiPeer() {
        super.addItemsToGuiPeer();

        this.guiPeerContainer.getChildren().addAll(this.leftRegion,
                this.recvButton,
                this.stopButton,
                this.optionsWindow.openOptionsButton);
    }
}