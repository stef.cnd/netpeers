package jpeersapp.frontend.generic_peers;

public enum GuiPeerState {
    IDLE,
    SENDING,
    RECEIVING
}
