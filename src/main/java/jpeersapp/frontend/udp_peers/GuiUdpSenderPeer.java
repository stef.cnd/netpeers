package jpeersapp.frontend.udp_peers;


import java.nio.charset.StandardCharsets;
import jpeersapp.backend.udp_peers.UdpSenderPeer;
import jpeersapp.backend.generic_peer.AbstractPeer;
import jpeersapp.frontend.generic_peers.GuiPeerState;
import jpeersapp.frontend.generic_peers.GuiSenderPeer;
import jpeersapp.frontend.tabs.peer_choice.PeerChoice;


/**
 * This class models the GUI equivalent of a UDP sender peer;
 */
public class GuiUdpSenderPeer extends GuiSenderPeer {

    private final UdpSenderPeer udpSenderPeer;

    
    /**
     * Constructor
     */
    public GuiUdpSenderPeer(Integer peerId, PeerChoice choice) {
        super(peerId, choice);

        this.udpSenderPeer = new UdpSenderPeer();

        /* Get the default sending mode and set the send/stop buttons */
        this.getSendingMode();
        this.setSendButton();
        this.setStopButton();
        this.udpSenderPeer.options.setSendMode("Custom Payload");
    }


    /**
     * Collect user input on UDP sender properties;
     * Set the UDP sender properties (backend);
     */
    protected void collectAndSetPeerProperties() {
        this.udpSenderPeer.setPeerAddress(this.addressField.getText());
        this.udpSenderPeer.setPeerPort(Integer.parseInt(this.portField.getText()));

        /* Set the sender buffer data according to the sending mode */
        switch (this.selectedSendingMode) {
            case "Custom Payload" -> {
                this.udpSenderPeer.setSendBufferData(this.senderPayloadField.getText().getBytes(StandardCharsets.UTF_8));
            }
            case "Incremental Counter" -> {
                String counter = this.optionsWindow.senderOptions.gui_getIncrementalCounter();
                this.udpSenderPeer.setSendBufferData(counter.getBytes(StandardCharsets.UTF_8));
            }
            case "Arbitrary Size" -> {
                String size = this.optionsWindow.senderOptions.gui_getArbitrarySize();
                this.udpSenderPeer.setArbitrarySize(Integer.parseInt(size));
            }
            default -> {
                System.out.println("WARNING: Sending mode not valid");
            }
        }
    }


    /**
     * Set the Send button actions and color behavior;
     */
    public void setSendButton() {
        this.sendButton.setOnAction(e -> {

            if (!this.state.equals(GuiPeerState.SENDING)) {
                this.sendButton.setStyle("-fx-text-fill: green");
                this.stopButton.setStyle("-fx-text-fill: black");

                this.collectAndSetPeerProperties();
                this.udpSenderPeer.initialiseSocket();
                this.udpSenderPeer.initialisePacket();
                this.udpSenderPeer.startSending();

                this.state = GuiPeerState.SENDING;
            }
        });
    }


    /**
     * Set the Stop button actions and color behavior;
     */
    private void setStopButton() {
        this.stopButton.setOnAction(e -> {

            if (!this.state.equals(GuiPeerState.IDLE)) {
                this.stopButton.setStyle("-fx-text-fill: red");
                this.sendButton.setStyle("-fx-text-fill: black");

                this.udpSenderPeer.closePeer();
                this.state = GuiPeerState.IDLE;
            }
        });
    }


    /**
     * Apply new options to the UDP sender peer (backend);
     */
    @Override
    public void applyNewOptions() {
        /* Get the selected sending mode: depending on
           this choice specific options will be set */
        this.getSendingMode();

        switch (this.selectedSendingMode) {
            case "Custom Payload" -> { /**/ }
            case "Incremental Counter" -> {
                this.udpSenderPeer.options.setIncrementalCounter((
                       Integer.parseInt(this.optionsWindow.senderOptions.gui_getIncrementalCounter())));
            }
            case "Arbitrary Size" -> { /**/ }
        }

        /* Apply the remaining options regardless of the sending mode */
        this.udpSenderPeer.options.setSendFrequency(this.optionsWindow.senderOptions.gui_getSendFrequency());
        this.udpSenderPeer.options.setSendMode(this.selectedSendingMode);
    }


    /**
     * Get the actual UDP sender backend peer encapsulated in the guiPeer;
     */
    @Override
    public AbstractPeer getPeer() {
        return this.udpSenderPeer;
    }
}