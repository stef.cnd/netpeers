package jpeersapp.frontend.tabs;


import java.io.InputStream;
import javafx.geometry.Pos;
import javafx.util.Duration;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.image.ImageView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import jpeersapp.frontend.options_window.OptionsWindow;
import jpeersapp.frontend.tabs.peer_choice.PeerChoice;
import jpeersapp.frontend.tabs.peer_choice.PeerChoiceBox;


/**
 * Abstract class that models a generic tab to be
 * shown in the main window;
 */
public abstract class GenericTab extends Tab {

    private PeerChoice peerChoice;
    private final HBox topBoxButtons;
    private final PeerChoiceBox peerChoiceBox;
    private final Button addPeerButton;
    private final Button removePeerButton;
    private final Button removeAllButton;
    protected final Button sendAllButton;
    protected final Button recvAllButton;
    protected final Button stopAllButton;
    protected final VBox mainWindowContainer;
    private final ListView<HBox> guiPeersView;
    private final ObservableList<HBox> guiPeersObservableList;
    protected ListView<String> logMsgsView;
    protected ObservableList<String> logMsgsObservableList;


    /**
     * Constructor
     */
    protected GenericTab(String tabName) {
        super(tabName);

        this.topBoxButtons = new HBox();
        this.mainWindowContainer = new VBox();
        this.addPeerButton = new Button();
        this.removePeerButton = new Button();
        this.removeAllButton = new Button();
        this.sendAllButton = new Button();
        this.recvAllButton = new Button();
        this.stopAllButton = new Button();
        this.peerChoice = PeerChoice.SENDER;
        this.peerChoiceBox = new PeerChoiceBox();
        this.guiPeersObservableList = FXCollections.observableArrayList();
        this.guiPeersView = new ListView<HBox>(this.guiPeersObservableList);
        this.logMsgsObservableList = FXCollections.observableArrayList();
        this.logMsgsView = new ListView<String>(this.logMsgsObservableList);


        this.setTopBoxButtons();
        this.setTopBoxButtonsLook();
        this.addItemsToTopBoxButtons();
        this.setLoggingArea();
    }


    /**
     * Set the behavior of the Peer ChoiceBox in the topBox container;
     */
    private void setPeerChoiceBox() {
        this.peerChoiceBox.getPeerChoiceBox().setOnAction(e -> {
            this.peerChoice = (PeerChoice) this.peerChoiceBox.getPeerChoiceBox().getValue();
        });
    }


    /**
     * Set the add peer button:
     * - Image
     * - Behavior
     * - Tooltip
     */
    private void setAddPeerButton() {
        InputStream input = OptionsWindow.class.getResourceAsStream("/icons/add.png");
        Image add = new Image(input, 18, 18, true, true);
        ImageView imageView = new ImageView(add);
        this.addPeerButton.setGraphic(imageView);

        this.addPeerButton.setOnAction(e -> {
            switch(this.peerChoice) {
                case SENDER -> addGuiSenderPeer(guiPeersObservableList);
                case RECEIVER -> addGuiReceiverPeer(guiPeersObservableList);
                case PAIR -> addGuiPair(guiPeersObservableList);
            }
        });

        Tooltip tooltip = new Tooltip("Add new peer");
        tooltip.setShowDelay(Duration.millis(400));
        this.addPeerButton.setTooltip(tooltip);
    }


    /**
     * Set the remove peer button:
     * - Image
     * - Behavior
     * - Tooltip
     */
    private void setRemovePeerButton() {
        InputStream input = OptionsWindow.class.getResourceAsStream("/icons/remove.png");
        Image remove = new Image(input, 18, 18, true, true);
        ImageView imageView = new ImageView(remove);
        this.removePeerButton.setGraphic(imageView);

        this.removePeerButton.setOnAction(e -> {
            removeGuiPeerPeer(guiPeersObservableList);
        });

        Tooltip tooltip = new Tooltip("Remove last peer");
        tooltip.setShowDelay(Duration.millis(400));
        this.removePeerButton.setTooltip(tooltip);
    }


    /**
     * Set the remove all peers button:
     * - Image
     * - Behavior
     * - Tooltip
     */
    private void setRemoveAllButton() {
        InputStream input = OptionsWindow.class.getResourceAsStream("/icons/remove_all.png");
        Image options = new Image(input, 18, 18, true, true);
        ImageView imageView = new ImageView(options);
        this.removeAllButton.setGraphic(imageView);

        this.removeAllButton.setOnAction(e -> {
            removeAllGuiPeers(guiPeersObservableList);
        });

        Tooltip tooltip = new Tooltip("Remove all peers");
        tooltip.setShowDelay(Duration.millis(400));
        this.removeAllButton.setTooltip(tooltip);
    }


    /**
     * Set the send all peers button:
     * - Image
     * - Tooltip
     */
    protected void setSendAllButton() {
        InputStream input = OptionsWindow.class.getResourceAsStream("/icons/send_all.png");
        Image options = new Image(input, 18, 18, true, true);
        ImageView imageView = new ImageView(options);
        this.sendAllButton.setGraphic(imageView);

        Tooltip tooltip = new Tooltip("Enable all senders");
        tooltip.setShowDelay(Duration.millis(400));
        this.sendAllButton.setTooltip(tooltip);
    }


    /**
     * Set the recv all peers button:
     * - Image
     * - Tooltip
     */
    protected void setRecvAllButton() {
        InputStream input = OptionsWindow.class.getResourceAsStream("/icons/receive_all.png");
        Image options = new Image(input, 18, 18, true, true);
        ImageView imageView = new ImageView(options);
        this.recvAllButton.setGraphic(imageView);

        Tooltip tooltip = new Tooltip("Enable all receivers");
        tooltip.setShowDelay(Duration.millis(400));
        this.recvAllButton.setTooltip(tooltip);
    }


    /**
     * Set the stop all peers button:
     * - Image
     * - Tooltip
     */
    protected void setStopAllButton() {
        InputStream input = OptionsWindow.class.getResourceAsStream("/icons/stop.png");
        Image options = new Image(input, 18, 18, true, true);
        ImageView imageView = new ImageView(options);
        this.stopAllButton.setGraphic(imageView);

        Tooltip tooltip = new Tooltip("Stop all peers");
        tooltip.setShowDelay(Duration.millis(400));
        this.stopAllButton.setTooltip(tooltip);
    }


    /**
     * Set the first top buttons in the topBox container;
     */
    protected void setTopBoxButtons() {
        this.setPeerChoiceBox();
        this.setAddPeerButton();
        this.setRemovePeerButton();
        this.setRemoveAllButton();
    }


    /**
     * Set the look of all buttons in the topBox container;
     */
    private void setTopBoxButtonsLook() {
        this.topBoxButtons.setSpacing(5);
        this.topBoxButtons.setMinHeight(28);
        this.topBoxButtons.setAlignment(Pos.CENTER_LEFT);
        this.topBoxButtons.setPadding(new Insets(5, 5, 5, 5));
    }


    /**
     * Add items to the topBox container;
     */
    private void addItemsToTopBoxButtons() {
        this.topBoxButtons.getChildren().addAll(this.peerChoiceBox.getPeerChoiceBox(),
                this.addPeerButton,
                this.removePeerButton,
                this.removeAllButton,
                this.sendAllButton,
                this.recvAllButton,
                this.stopAllButton);
    }


    /**
     * Add items to the main window container;
     */
    private void setLoggingArea() {
        this.logMsgsView.getStyleClass().add("one-color-list");
        this.logMsgsView.setPrefHeight(250);
    }


    /**
     * Add items to the main window container;
     */
    protected void addItemsToWindowContainer() {
        this.mainWindowContainer.getChildren().addAll(this.topBoxButtons,
                this.guiPeersView);
        setContent(this.mainWindowContainer);
    }


    /**
     * Add a sender peer to the tab list;
     */
    protected abstract void addGuiSenderPeer(ObservableList<HBox> peers);


    /**
     * Add a receiver peer to the tab list;
     */
    protected abstract void addGuiReceiverPeer(ObservableList<HBox> peers);


    /**
     * Remove a peer from the tab list;
     */
    protected abstract void removeGuiPeerPeer(ObservableList<HBox> peers);


    /**
     * Add a peer pair to the tab list;
     */
    protected void addGuiPair(ObservableList<HBox> peers) {
        addGuiSenderPeer(peers);
        addGuiReceiverPeer(peers);
    }


    /**
     * Remove a peer pair from the tab list;
     */
    protected void removeAllGuiPeers(ObservableList<HBox> peers) {
        if (peers.size() > 0) {
            peers.clear();
        }
    }
}