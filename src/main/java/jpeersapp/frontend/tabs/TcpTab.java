package jpeersapp.frontend.tabs;


import java.util.List;
import java.io.Closeable;
import java.io.InputStream;
import java.util.ArrayList;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.collections.ObservableList;
import jpeersapp.frontend.generic_peers.GuiPeer;
import jpeersapp.frontend.options_window.OptionsWindow;
import jpeersapp.frontend.tabs.logging.TcpAppender;
import jpeersapp.frontend.tabs.peer_choice.PeerChoice;
import jpeersapp.frontend.tcp_peers.GuiTcpReceiverPeer;
import jpeersapp.frontend.tcp_peers.GuiTcpSenderPeer;


/**
 * Class that models the TCP tab to be shown in the
 * main window;
 */
public class TcpTab extends GenericTab {

    protected Integer peerCounter;
    private final List<GuiPeer> tcpPeers;


    /**
     * Constructor
     */
    public TcpTab(String tabName) {
        super(tabName);

        this.peerCounter = 1;
        this.tcpPeers = new ArrayList<>();
        this.addItemsToWindowContainer();

        TcpAppender.setLogArea(this.logMsgsObservableList, this.logMsgsView);
        this.printWelcomeMessage();
    }


    /**
     * Add a sender peer to the TCP tab list;
     */
    @Override
    protected void addGuiSenderPeer(ObservableList<HBox> peers) {
        GuiTcpSenderPeer guiSenderPeer = new GuiTcpSenderPeer(this.peerCounter++, PeerChoice.SENDER);
        guiSenderPeer.addItemsToGuiPeer();
        peers.addAll(guiSenderPeer.getGuiPeerContainer());

        this.tcpPeers.add(guiSenderPeer);
    }


    /**
     * Add a receiver peer to the UDP tab list;
     */
    @Override
    protected void addGuiReceiverPeer(ObservableList<HBox> peers) {
        GuiTcpReceiverPeer guiReceiverPeer = new GuiTcpReceiverPeer(this.peerCounter++, PeerChoice.RECEIVER);
        guiReceiverPeer.addItemsToGuiPeer();
        peers.addAll(guiReceiverPeer.getGuiPeerContainer());

        this.tcpPeers.add(guiReceiverPeer);
    }


    /**
     * Remove a peer from the TCP tab list;
     */
    @Override
    protected void removeGuiPeerPeer(ObservableList<HBox> peers) {
        if (peers.size() > 0) {

            if (this.tcpPeers.size() > 0) {
                Closeable socket = this.tcpPeers.get(this.tcpPeers.size()-1).getPeer().getSocket();
                this.tcpPeers.get(this.tcpPeers.size()-1).getPeer().closePeer(socket);
                this.tcpPeers.remove(this.tcpPeers.get(this.tcpPeers.size()-1));
            }

            this.peerCounter--;
            peers.remove(peers.size() - 1);
        }
    }


    /**
     * Remove all peers from the TCP tab list;
     */
    @Override
    protected void removeAllGuiPeers(ObservableList<HBox> peers) {
        if (this.tcpPeers.size() > 0) {
            for (GuiPeer guiPeer : this.tcpPeers) {
                Closeable socket = guiPeer.getPeer().getSocket();
                guiPeer.getPeer().closePeer(socket);
            }

            this.tcpPeers.clear();
        }

        super.removeAllGuiPeers(peers);
        this.peerCounter = 1;
    }


    /**
     * Set the send all peers button behavior;
     */
    @Override
    protected void setSendAllButton() {
        super.setSendAllButton();

        this.sendAllButton.setOnAction(e -> {
            if (!this.tcpPeers.isEmpty()) {
                InputStream input = OptionsWindow.class.getResourceAsStream("/icons/send_all_on.png");
                Image options = new Image(input, 18, 18, true, true);
                ImageView imageView = new ImageView(options);
                this.sendAllButton.setGraphic(imageView);
            }

            for (GuiPeer sender : this.tcpPeers) {
                if (sender.choice.equals(PeerChoice.SENDER)) {
                    sender.sendButton.fire();
                }
            }
        });
    }


    /**
     * Set the recv all peers button behavior;
     */
    @Override
    protected void setRecvAllButton() {
        super.setRecvAllButton();

        this.recvAllButton.setOnAction(e -> {
            if (!(this.tcpPeers.isEmpty())) {
                InputStream input = OptionsWindow.class.getResourceAsStream("/icons/receive_all_on.png");
                Image options = new Image(input, 18, 18, true, true);
                ImageView imageView = new ImageView(options);
                this.recvAllButton.setGraphic(imageView);
            }

            for (GuiPeer receiver : this.tcpPeers) {
                if (receiver.choice.equals(PeerChoice.RECEIVER)) {
                    receiver.recvButton.fire();
                }
            }
        });
    }


    /**
     * Set the stop all peers button behavior;
     */
    @Override
    protected void setStopAllButton() {
        super.setStopAllButton();

        this.stopAllButton.setOnAction(e -> {

            /* We need to reset the sendAll and receiveAll buttons color */
            super.setSendAllButton();
            super.setRecvAllButton();

            for (GuiPeer peer : this.tcpPeers) {
                peer.stopButton.fire();
            }
        });
    }


    /**
     * Set the send/recv/stop all buttons in the topBox container;
     */
    @Override
    protected void setTopBoxButtons() {
        super.setTopBoxButtons();

        this.setSendAllButton();
        this.setRecvAllButton();
        this.setStopAllButton();
    }


    /**
     * Add items to the main window container;
     */
    protected void addItemsToWindowContainer() {
        super.addItemsToWindowContainer();
        this.mainWindowContainer.getChildren().addAll(this.logMsgsView);
        setContent(this.mainWindowContainer);
    }


    /**
     * Print a welcome message in the Tcp logging area;
     */
    private void printWelcomeMessage() {
        this.logMsgsObservableList.addAll("Welcome to JPeers 1.1.\n");
    }
}