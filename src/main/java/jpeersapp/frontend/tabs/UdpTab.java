package jpeersapp.frontend.tabs;


import java.util.*;
import java.io.Closeable;
import java.io.InputStream;
import javafx.scene.layout.HBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.collections.ObservableList;
import jpeersapp.frontend.generic_peers.GuiPeer;
import jpeersapp.frontend.options_window.OptionsWindow;
import jpeersapp.frontend.tabs.logging.UdpAppender;
import jpeersapp.frontend.tabs.peer_choice.PeerChoice;
import jpeersapp.frontend.udp_peers.GuiUdpReceiverPeer;
import jpeersapp.frontend.udp_peers.GuiUdpSenderPeer;


/**
 * Class that models the UDP tab to be shown in the
 * main window;
 */
public class UdpTab extends GenericTab {

    private Integer peerCounter;
    private final List<GuiPeer> udpPeers;


    /**
     * Constructor
     */
    public UdpTab(String tabName) {
        super(tabName);

        this.peerCounter = 1;
        this.udpPeers = new ArrayList<>();
        this.setTopBoxButtons();
        this.addItemsToWindowContainer();

        UdpAppender.setLogArea(this.logMsgsObservableList, this.logMsgsView);
        this.printWelcomeMessage();
    }


    /**
     * Add a sender peer to the UDP tab list;
     */
    @Override
    protected void addGuiSenderPeer(ObservableList<HBox> peers) {
        GuiUdpSenderPeer guiSenderPeer = new GuiUdpSenderPeer(this.peerCounter++, PeerChoice.SENDER);
        guiSenderPeer.addItemsToGuiPeer();
        peers.addAll(guiSenderPeer.getGuiPeerContainer());

        this.udpPeers.add(guiSenderPeer);
    }


    /**
     * Add a receiver peer to the UDP tab list;
     */
    @Override
    protected void addGuiReceiverPeer(ObservableList<HBox> peers) {
        GuiUdpReceiverPeer guiReceiverPeer = new GuiUdpReceiverPeer(this.peerCounter++, PeerChoice.RECEIVER);
        guiReceiverPeer.addItemsToGuiPeer();
        peers.addAll(guiReceiverPeer.getGuiPeerContainer());

        this.udpPeers.add(guiReceiverPeer);
    }


    /**
     * Remove a peer from the UDP tab list;
     */
    @Override
    protected void removeGuiPeerPeer(ObservableList<HBox> peers) {
        if (peers.size() > 0) {

            if (this.udpPeers.size() > 0) {
                Closeable socket = this.udpPeers.get(this.udpPeers.size()-1).getPeer().getSocket();
                this.udpPeers.get(this.udpPeers.size()-1).getPeer().closePeer(socket);
                this.udpPeers.remove(this.udpPeers.get(this.udpPeers.size()-1));
            }

            this.peerCounter--;
            peers.remove(peers.size() - 1);
        }
    }


    /**
     * Remove all peers from the UDP tab list;
     */
    @Override
    protected void removeAllGuiPeers(ObservableList<HBox> peers) {


        if (this.udpPeers.size() > 0) {
            for (GuiPeer guiPeer : this.udpPeers) {
                Closeable socket = guiPeer.getPeer().getSocket();
                guiPeer.getPeer().closePeer(socket);
            }

            this.udpPeers.clear();
        }

        super.removeAllGuiPeers(peers);
        this.peerCounter = 1;
    }


    /**
     * Set the send all peers button behavior;
     */
    @Override
    protected void setSendAllButton() {
        super.setSendAllButton();

        this.sendAllButton.setOnAction(e -> {
            if (!this.udpPeers.isEmpty()) {
                InputStream input = OptionsWindow.class.getResourceAsStream("/icons/send_all_on.png");
                Image options = new Image(input, 18, 18, true, true);
                ImageView imageView = new ImageView(options);
                this.sendAllButton.setGraphic(imageView);

                for (GuiPeer sender : this.udpPeers) {
                    if (sender.choice.equals(PeerChoice.SENDER)) {
                        sender.sendButton.fire();
                    }
                }
            }
        });
    }


    /**
     * Set the recv all peers button behavior;
     */
    @Override
    protected void setRecvAllButton() {
        super.setRecvAllButton();

        this.recvAllButton.setOnAction(e -> {
            if (!(this.udpPeers.isEmpty())) {
                InputStream input = OptionsWindow.class.getResourceAsStream("/icons/receive_all_on.png");
                Image options = new Image(input, 18, 18, true, true);
                ImageView imageView = new ImageView(options);
                this.recvAllButton.setGraphic(imageView);

                for (GuiPeer receiver : this.udpPeers) {
                    if (receiver.choice.equals(PeerChoice.RECEIVER)) {
                        receiver.recvButton.fire();
                    }
                }
            }
        });
    }


    /**
     * Set the stop all peers button behavior;
     */
    @Override
    protected void setStopAllButton() {
        super.setStopAllButton();

        this.stopAllButton.setOnAction(e -> {

            /* We need to reset the sendAll and receiveAll buttons color */
            super.setSendAllButton();
            super.setRecvAllButton();

            for (GuiPeer peer : this.udpPeers) {
                peer.stopButton.fire();
            }
        });
    }


    /**
     * Set the send/recv/stop all buttons in the topBox container;
     */
    @Override
    protected void setTopBoxButtons() {
        super.setTopBoxButtons();

        this.setSendAllButton();
        this.setRecvAllButton();
        this.setStopAllButton();
    }


    /**
     * Add items to the main window container;
     */
    protected void addItemsToWindowContainer() {
        super.addItemsToWindowContainer();
        this.mainWindowContainer.getChildren().addAll(this.logMsgsView);
        setContent(this.mainWindowContainer);
    }


    /**
     * Print a welcome message in the Udp logging area;
     */
    private void printWelcomeMessage() {
        this.logMsgsObservableList.addAll("Welcome to JPeers 1.1.\n");
    }
}