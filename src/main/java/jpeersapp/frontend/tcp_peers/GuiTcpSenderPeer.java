package jpeersapp.frontend.tcp_peers;


import java.nio.charset.StandardCharsets;
import jpeersapp.backend.tcp_peers.TcpSenderPeer;
import jpeersapp.backend.generic_peer.AbstractPeer;
import jpeersapp.frontend.generic_peers.GuiPeerState;
import jpeersapp.frontend.generic_peers.GuiSenderPeer;
import jpeersapp.frontend.tabs.peer_choice.PeerChoice;


/**
 * This class models the GUI equivalent of a TCP sender peer;
 */
public class GuiTcpSenderPeer extends GuiSenderPeer {

    private final TcpSenderPeer tcpSenderPeer;


    /**
     * Constructor
     */
    public GuiTcpSenderPeer(Integer peerId, PeerChoice choice) {
        super(peerId, choice);

        this.tcpSenderPeer = new TcpSenderPeer();

        /* Get the default sending mode and set the send/stop buttons */
        this.getSendingMode();
        this.setSendButton();
        this.setStopButton();
        this.tcpSenderPeer.options.setSendMode("Custom Payload");
    }


    /**
     * Collect user input on TCP sender properties;
     * Set the TCP sender properties (backend);
     */
    protected void collectAndSetPeerProperties() {
        this.tcpSenderPeer.setPeerAddress(this.addressField.getText());
        this.tcpSenderPeer.setPeerPort(Integer.parseInt(this.portField.getText()));

        /* Set the sender buffer data according to the sending mode */
        switch (this.selectedSendingMode) {
            case "Custom Payload" -> {
                this.tcpSenderPeer.setSendBufferData(this.senderPayloadField.getText().getBytes(StandardCharsets.UTF_8));
            }
            case "Incremental Counter" -> {
                String counter = this.optionsWindow.senderOptions.gui_getIncrementalCounter();
                this.tcpSenderPeer.setSendBufferData(counter.getBytes(StandardCharsets.UTF_8));
            }
            case "Arbitrary Size" -> {
                String size = this.optionsWindow.senderOptions.gui_getArbitrarySize();
                this.tcpSenderPeer.setArbitrarySize(Integer.parseInt(size));
            }
            default -> {
                System.out.println("WARNING: Sending mode not valid");
            }
        }

    }


    /**
     * Set the Send button actions and color behavior;
     */
    private void setSendButton() {
        this.sendButton.setOnAction(e -> {

            if (!this.state.equals(GuiPeerState.SENDING)) {
                this.sendButton.setStyle("-fx-text-fill: green");
                this.stopButton.setStyle("-fx-text-fill: black");

                this.collectAndSetPeerProperties();
                this.tcpSenderPeer.initialiseSocket();
                this.tcpSenderPeer.startSending();

                this.state = GuiPeerState.SENDING;
            }
        });
    }


    /**
     * Set the Stop button actions and color behavior;
     */
    private void setStopButton() {
        this.stopButton.setOnAction(e -> {

            if (!this.state.equals(GuiPeerState.IDLE)) {
                this.stopButton.setStyle("-fx-text-fill: red");
                this.sendButton.setStyle("-fx-text-fill: black");

                this.tcpSenderPeer.closePeer();
                this.state = GuiPeerState.IDLE;
            }
        });
    }


    /**
     * Apply new options to the TCP sender peer (backend);
     */
    @Override
    public void applyNewOptions() {
        /* Get the selected sending mode: depending on
           this choice specific options will be set */
        this.getSendingMode();

        switch (this.selectedSendingMode) {
            case "Custom Payload" -> { /**/ }
            case "Incremental Counter" -> {
                this.tcpSenderPeer.options.setIncrementalCounter((
                        Integer.parseInt(this.optionsWindow.senderOptions.gui_getIncrementalCounter())));
            }
            case "Arbitrary Size" -> { /**/ }
        }

        this.tcpSenderPeer.options.setSendFrequency(this.optionsWindow.senderOptions.gui_getSendFrequency());
        this.tcpSenderPeer.options.setSendMode(this.selectedSendingMode);
    }


    /**
     * Get the actual UDP sender backend peer encapsulated in the guiPeer;
     */
    @Override
    public AbstractPeer getPeer() {
        return this.tcpSenderPeer;
    }
}